# Flux Pastell Actes (automatique) "Gironde Numérique" pour Pastell V2

## Description
Surcharge du flux général "Actes automatique" afin de prendre en compte le connecteur "Données citoyennes" dans son flux de transmission automatique.

## Pré-requis
- Extension Apache Solr
- Compte utilisateur Insee Sirene V3
- Serveur Solr
- Pastell V2
- Connecteur "données citoyennes"

## Installation
- Installer le connecteur "Données citoyennes" en premier puis le configurer.
- Déposer le dossier du flux dans le répertoire /data/extensions/ de l'instance Pastell.
