<?php 

/**
 * Send documents to Citizen data (file on a OpenData server and index in a Solr server) from automatic flux GN
 *
 *
 * @license    http://cecill.info/licences/Licence_CeCILL_V2-fr.html  Cecill V2
 * @version    0.3
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class IndexeurEnvoi extends ActionExecutor {
	
	public function go() {
		$genericActForm = $this->getDonneesFormulaire();

		/**
		 * Transfer to citizen data eligible for : 
		 * 1 - deliberations, 
		 * 2 - regulatory acts
		 * 5 - budget and financial documents
		 */ 
		$natureAct = $genericActForm->get('acte_nature');
		if ($natureAct == 1 || $natureAct == 2 || $natureAct == 5) {
			$indexeur = $this->getConnecteur('Indexeur');

			// Get entity informations
			$siren =  $this->objectInstancier->EntiteSQL->getSiren($this->id_e);
			$organization = $indexeur->sireneConsumer->getOrganizationInformations($siren);

			if ($organization != NULL) {
				// Get document from generic act form
				$document = $indexeur->getDocumentFromForm($genericActForm, $organization);
				$document->setOrigin('publication_acte_pastell');

				if ($indexeur->indexDocument($document, false)) {
					$this->addActionOK('Acte envoyé vers données citoyennes');
					$this->setLastMessage('L\'acte a bien été déposé dans les données citoyennes');
					$this->notify($this->action, $this->type, $this->getLastMessage());
					return true;
				} else {
					$this->setLastError('L\'envoi vers les données citoyennes a échoué : '.$indexeur->getLastError());
					return false;
				}
			} else {
				$this->setLastError('Impossible de récupérer les informations de l\'entité');
				return false;
			}
			
		} else {
            $this->setLastMessage('Cet acte n\'est pas éligible au versement aux données citoyennes');
            return false;
        }
		
		return true;
	}
	
}

	